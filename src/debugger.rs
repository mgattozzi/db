use breakpoint::BreakPoint;
use failure::Error;
use libc::c_void;
use nix::{sys::{ptrace::{ptrace, Request},
                wait::{waitpid, WaitStatus}},
          unistd::Pid};
use registers::*;
use rustyline::error::ReadlineError;
use rustyline::Editor;
use std::{collections::HashMap, ptr};

pub struct Debugger {
    child_pid: Pid,
    _prog_name: String,
    break_points: HashMap<usize, BreakPoint>,
}

impl Debugger {
    pub fn new(child_pid: Pid, _prog_name: String) -> Self {
        Self {
            child_pid,
            _prog_name,
            break_points: HashMap::new(),
        }
    }

    // TODO restructure error handling so that the buck stops here
    pub fn run(mut self) -> Result<(), Error> {
        match waitpid(self.child_pid, None)? {
            WaitStatus::Stopped(_, _) => {}
            _ => bail!("Invalid program path passed in. db failed to start"),
        }

        let mut rl = Editor::<()>::new();
        loop {
            let readline = rl.readline(">> ");
            let _ = rl.load_history("history");
            match readline {
                Ok(line) => {
                    rl.add_history_entry(&line);
                    self.handle_command(line)?;
                }
                Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => {
                    break;
                }
                Err(err) => {
                    bail!(err);
                }
            }
        }
        rl.save_history("history")?;
        Ok(())
    }

    fn handle_command(&mut self, line: String) -> Result<(), Error> {
        // TODO handle the stringiness better by wrapping in types
        let mut command = line.split_whitespace();
        match command.next().unwrap_or("empty command") {
            "continue" | "c" => self.continue_exec(),
            "break" | "b" => self.break_exec(command.next().unwrap_or("no address")),
            "register" | "r" => {
                match command.next().unwrap_or("no command") {
                    "dump" => self.dump_registers(),
                    "read" => {
                        // Incorrect register names cause early termination here
                        println!(
                            "0x{:016x}",
                            get_register_value(
                                self.child_pid,
                                get_register_by_name(command.next().unwrap_or(""))?
                            )?
                        );
                        Ok(())
                    }
                    "write" => {
                        let name = command.next().unwrap_or("");
                        // We're assuming they type in 0x here
                        let value =
                            u64::from_str_radix(&command.next().unwrap_or("OH NO")[2..], 16)?;
                        // Incorrect register names cause early termination here
                        set_register_value(self.child_pid, get_register_by_name(name)?, value)?;
                        Ok(())
                    }
                    "no command" => {
                        println!("register command needs a subcommand to work");
                        Ok(())
                    }
                    c => {
                        println!("{} is an invalid subcommand for register", c);
                        Ok(())
                    }
                }
            }
            "memory" | "m" => {
                match command.next().unwrap_or("no command") {
                    "read" => {
                        // We're assuming they type in 0x here
                        let address =
                            usize::from_str_radix(&command.next().unwrap_or("OH NO")[2..], 16)?;
                        println!("0x{:016x}", self.read_memory(address)?);
                        Ok(())
                    }
                    "write" => {
                        // We're assuming they type in 0x here
                        let address =
                            usize::from_str_radix(&command.next().unwrap_or("OH NO")[2..], 16)?;
                        // We're assuming they type in 0x here
                        let value =
                            usize::from_str_radix(&command.next().unwrap_or("OH NO")[2..], 16)?;
                        // Incorrect register names cause early termination here
                        self.write_memory(address, value)?;
                        Ok(())
                    }
                    "no command" => {
                        println!("memory command needs a subcommand to work");
                        Ok(())
                    }
                    c => {
                        println!("{} is an invalid memory for register", c);
                        Ok(())
                    }
                }
            }
            "empty command" => {
                println!("Invalid command");
                Ok(())
            }
            c => {
                println!("{} is an invalid command", c);
                Ok(())
            }
        }
    }

    fn break_exec(&mut self, address: &str) -> Result<(), Error> {
        if address == "no address" {
            println!("No address passed to break command");
        } else {
            // drop the 0x from the string then parse the isize from base hex
            self.set_break_point_at_address(usize::from_str_radix(&address[2..], 16)?)?;
        }
        Ok(())
    }

    fn continue_exec(&mut self) -> Result<(), Error> {
        let null = ptr::null_mut::<c_void>();
        let null2 = ptr::null_mut::<c_void>();
        self.step_over_breakpoint()?;
        unsafe { ptrace(Request::PTRACE_CONT, self.child_pid, null, null2) }?;
        wait_for_signal(self.child_pid)?;
        Ok(())
    }

    fn set_break_point_at_address(&mut self, ptr: usize) -> Result<(), Error> {
        let mut break_point = BreakPoint::new(self.child_pid, ptr);
        break_point.enable()?;
        self.break_points.insert(ptr, break_point);
        println!("Set breakpoint at address 0x{:x}", ptr);
        Ok(())
    }

    fn dump_registers(&mut self) -> Result<(), Error> {
        for r in G_REGISTER_DESCRIPTORS.iter() {
            let reg_val = get_register_value(self.child_pid, r.reg)?;
            let mut name = format!("{}: ", r.name);
            // The longest name length is 'orig_rax:' so we need to use 10 to properly buffer
            // output
            for _ in 0..(10 - name.len()) {
                name.push(' ');
            }
            println!("{}0x{:016x}", name, reg_val);
        }
        Ok(())
    }

    fn read_memory(&mut self, address: usize) -> Result<usize, Error> {
        let null = ptr::null_mut::<c_void>();
        let addr = address as *mut c_void;
        Ok(unsafe { ptrace(Request::PTRACE_PEEKDATA, self.child_pid, addr, null)? as usize })
    }

    fn write_memory(&mut self, address: usize, value: usize) -> Result<(), Error> {
        let addr = address as *mut c_void;
        let _ = unsafe {
            ptrace(
                Request::PTRACE_POKEDATA,
                self.child_pid,
                addr,
                value as *mut c_void,
            )
        }?;
        Ok(())
    }

    fn step_over_breakpoint(&mut self) -> Result<(), Error> {
        // - 1 because execution will go past the breakpoint
        let possible_breakpoint_location = get_pc(self.child_pid)? - 1;
        let mut value = self.break_points
            .get_mut(&(possible_breakpoint_location as usize));
        match value {
            Some(ref mut bp) => {
                if bp.is_enabled() {
                    let previous_instruction_address = possible_breakpoint_location;
                    set_pc(self.child_pid, previous_instruction_address)?;
                    bp.disable()?;
                    // Do we need two null ptrs? I don't know and I don't want to find out
                    let null = ptr::null_mut::<c_void>();
                    let null2 = ptr::null_mut::<c_void>();
                    unsafe { ptrace(Request::PTRACE_SINGLESTEP, self.child_pid, null, null2) }?;
                    wait_for_signal(self.child_pid)?;
                    bp.enable()?;
                }
            }
            None => {}
        }

        Ok(())
    }
}

// Mutability means I have to do this.
fn wait_for_signal(pid: Pid) -> Result<(), Error> {
    match waitpid(pid, None)? {
        _ => Ok(()),
    }
}

fn get_pc(pid: Pid) -> Result<u64, Error> {
    get_register_value(pid, Register::RIP)
}

fn set_pc(pid: Pid, pc: u64) -> Result<(), Error> {
    set_register_value(pid, Register::RIP, pc)
}
