use failure::Error;
use libc::{c_void, user_regs_struct};
use nix::{sys::ptrace::{ptrace, Request},
          unistd::Pid};
use std::{mem, ptr};

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum Register {
    RAX,
    RBX,
    RCX,
    RDX,
    RDI,
    RSI,
    RBP,
    RSP,
    R8,
    R9,
    R10,
    R11,
    R12,
    R13,
    R14,
    R15,
    RIP,
    RFLAGS,
    CS,
    OrigRax,
    FsBase,
    GsBase,
    FS,
    GS,
    SS,
    DS,
    ES,
}

pub const N_REGISTERS: usize = 27;

pub struct RegDescriptor {
    pub reg: Register,
    pub dwarf_r: i32,
    pub name: &'static str,
}

pub const G_REGISTER_DESCRIPTORS: [RegDescriptor; N_REGISTERS] = [
    RegDescriptor {
        reg: Register::R15,
        dwarf_r: 15,
        name: "r15",
    },
    RegDescriptor {
        reg: Register::R14,
        dwarf_r: 14,
        name: "r14",
    },
    RegDescriptor {
        reg: Register::R13,
        dwarf_r: 13,
        name: "r13",
    },
    RegDescriptor {
        reg: Register::R12,
        dwarf_r: 12,
        name: "r12",
    },
    RegDescriptor {
        reg: Register::RBP,
        dwarf_r: 6,
        name: "rbp",
    },
    RegDescriptor {
        reg: Register::RBX,
        dwarf_r: 3,
        name: "rbx",
    },
    RegDescriptor {
        reg: Register::R11,
        dwarf_r: 11,
        name: "r11",
    },
    RegDescriptor {
        reg: Register::R10,
        dwarf_r: 10,
        name: "r10",
    },
    RegDescriptor {
        reg: Register::R9,
        dwarf_r: 9,
        name: "r9",
    },
    RegDescriptor {
        reg: Register::R8,
        dwarf_r: 8,
        name: "r8",
    },
    RegDescriptor {
        reg: Register::RAX,
        dwarf_r: 0,
        name: "rax",
    },
    RegDescriptor {
        reg: Register::RCX,
        dwarf_r: 2,
        name: "rcx",
    },
    RegDescriptor {
        reg: Register::RDX,
        dwarf_r: 1,
        name: "rdx",
    },
    RegDescriptor {
        reg: Register::RSI,
        dwarf_r: 4,
        name: "rsi",
    },
    RegDescriptor {
        reg: Register::RDI,
        dwarf_r: 5,
        name: "rdi",
    },
    RegDescriptor {
        reg: Register::OrigRax,
        dwarf_r: -1,
        name: "orig_rax",
    },
    RegDescriptor {
        reg: Register::RIP,
        dwarf_r: -1,
        name: "rip",
    },
    RegDescriptor {
        reg: Register::CS,
        dwarf_r: 51,
        name: "cs",
    },
    RegDescriptor {
        reg: Register::RFLAGS,
        dwarf_r: 49,
        name: "eflags",
    },
    RegDescriptor {
        reg: Register::RSP,
        dwarf_r: 7,
        name: "rsp",
    },
    RegDescriptor {
        reg: Register::SS,
        dwarf_r: 52,
        name: "ss",
    },
    RegDescriptor {
        reg: Register::FsBase,
        dwarf_r: 58,
        name: "fs_base",
    },
    RegDescriptor {
        reg: Register::GsBase,
        dwarf_r: 59,
        name: "gs_base",
    },
    RegDescriptor {
        reg: Register::DS,
        dwarf_r: 53,
        name: "ds",
    },
    RegDescriptor {
        reg: Register::ES,
        dwarf_r: 50,
        name: "es",
    },
    RegDescriptor {
        reg: Register::FS,
        dwarf_r: 54,
        name: "fs",
    },
    RegDescriptor {
        reg: Register::GS,
        dwarf_r: 55,
        name: "gs",
    },
];

pub fn get_register_value(pid: Pid, r: Register) -> Result<u64, Error> {
    let mut regs: user_regs_struct = unsafe { mem::uninitialized() };
    let _ = unsafe {
        ptrace(
            Request::PTRACE_GETREGS,
            pid,
            ptr::null_mut::<c_void>(),
            &mut regs as *mut user_regs_struct as *mut c_void,
        )
    }?;
    let reg_val = match r {
        Register::R15 => regs.r15,
        Register::R14 => regs.r14,
        Register::R13 => regs.r15,
        Register::R12 => regs.r12,
        Register::RBP => regs.rbp,
        Register::RBX => regs.rbx,
        Register::R11 => regs.r11,
        Register::R10 => regs.r10,
        Register::R9 => regs.r9,
        Register::R8 => regs.r8,
        Register::RAX => regs.rax,
        Register::RCX => regs.rcx,
        Register::RDX => regs.rdx,
        Register::RSI => regs.rsi,
        Register::RDI => regs.rdi,
        Register::OrigRax => regs.orig_rax,
        Register::RIP => regs.rip,
        Register::CS => regs.cs,
        Register::RFLAGS => regs.eflags,
        Register::RSP => regs.rsp,
        Register::SS => regs.ss,
        Register::FsBase => regs.fs_base,
        Register::GsBase => regs.gs_base,
        Register::DS => regs.ds,
        Register::ES => regs.es,
        Register::FS => regs.fs,
        Register::GS => regs.gs,
    };

    Ok(reg_val)
}

pub fn set_register_value(pid: Pid, r: Register, value: u64) -> Result<(), Error> {
    let mut regs: user_regs_struct = unsafe { mem::uninitialized() };
    let _ = unsafe {
        ptrace(
            Request::PTRACE_GETREGS,
            pid,
            ptr::null_mut::<c_void>(),
            &mut regs as *mut user_regs_struct as *mut c_void,
        )
    }?;
    match r {
        Register::R15 => regs.r15 = value,
        Register::R14 => regs.r14 = value,
        Register::R13 => regs.r15 = value,
        Register::R12 => regs.r12 = value,
        Register::RBP => regs.rbp = value,
        Register::RBX => regs.rbx = value,
        Register::R11 => regs.r11 = value,
        Register::R10 => regs.r10 = value,
        Register::R9 => regs.r9 = value,
        Register::R8 => regs.r8 = value,
        Register::RAX => regs.rax = value,
        Register::RCX => regs.rcx = value,
        Register::RDX => regs.rdx = value,
        Register::RSI => regs.rsi = value,
        Register::RDI => regs.rdi = value,
        Register::OrigRax => regs.orig_rax = value,
        Register::RIP => regs.rip = value,
        Register::CS => regs.cs = value,
        Register::RFLAGS => regs.eflags = value,
        Register::RSP => regs.rsp = value,
        Register::SS => regs.ss = value,
        Register::FsBase => regs.fs_base = value,
        Register::GsBase => regs.gs_base = value,
        Register::DS => regs.ds = value,
        Register::ES => regs.es = value,
        Register::FS => regs.fs = value,
        Register::GS => regs.gs = value,
    }
    let _ = unsafe {
        ptrace(
            Request::PTRACE_SETREGS,
            pid,
            ptr::null_mut::<c_void>(),
            &mut regs as *mut user_regs_struct as *mut c_void,
        )
    }?;
    Ok(())
}

pub fn get_register_value_from_dwarf_register(pid: Pid, regnum: i32) -> Result<u64, Error> {
    match G_REGISTER_DESCRIPTORS.iter().find(|i| i.dwarf_r == regnum) {
        Some(val) => get_register_value(pid, val.reg),
        None => bail!("Invalid DWARF Register number used"),
    }
}

pub fn get_register_name(reg: Register) -> &'static str {
    // We already know that all the registers are part of the global var as a lookup so we will
    // find it and can unwrap accordingly
    G_REGISTER_DESCRIPTORS
        .iter()
        .find(|i| i.reg == reg)
        .unwrap()
        .name
}

pub fn get_register_by_name(name: &str) -> Result<Register, Error> {
    match G_REGISTER_DESCRIPTORS.iter().find(|i| i.name == name) {
        Some(val) => Ok(val.reg),
        None => bail!("The name used to get the Register value is invalid."),
    }
}
