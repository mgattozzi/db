use failure::Error;
use libc::c_void;
use nix::{sys::ptrace::{ptrace, Request},
          unistd::Pid};
use std::ptr;

pub struct BreakPoint {
    pid: Pid,
    addr: usize,
    enabled: bool,
    saved_data: Option<u8>,
}

impl BreakPoint {
    pub fn new(pid: Pid, addr: usize) -> Self {
        Self {
            pid,
            addr,
            enabled: false,
            saved_data: None,
        }
    }
    pub fn is_enabled(&self) -> bool {
        self.enabled
    }
    pub fn get_address(&self) -> usize {
        self.addr
    }

    pub fn enable(&mut self) -> Result<(), Error> {
        let null = ptr::null_mut::<c_void>();
        let addr = self.addr as *mut c_void;
        let data = unsafe { ptrace(Request::PTRACE_PEEKDATA, self.pid, addr, null) }?;

        self.saved_data = Some((data & 0xff) as u8); // Save bottom byte
        let int3 = 0xcc as u64;
        let data_with_int3 = (data & !0xff) as u64 | int3; // Set bottom byte to 0xcc
        let _ = unsafe {
            ptrace(
                Request::PTRACE_POKEDATA,
                self.pid,
                addr,
                data_with_int3 as *mut c_void,
            )
        }?;
        self.enabled = true;

        Ok(())
    }

    pub fn disable(&mut self) -> Result<(), Error> {
        let null = ptr::null::<c_void>();
        let addr = self.addr as *mut c_void;
        let data = unsafe {
            ptrace(
                Request::PTRACE_PEEKDATA,
                self.pid,
                addr,
                null as *mut c_void,
            )
        }?;
        match self.saved_data {
            Some(saved_data) => {
                let restored_data = (data & !0xff) as u64 | saved_data as u64;
                let _ = unsafe {
                    ptrace(
                        Request::PTRACE_POKEDATA,
                        self.pid,
                        addr,
                        restored_data as *mut c_void,
                    )
                }?;
                self.enabled = false;
            }
            None => {}
        }
        Ok(())
    }
}
