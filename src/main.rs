// We need this for a few ptrace functions that have not been implemented by nix
#![allow(deprecated)]
// Until we get to the end of the debugger series supress
#![allow(unused)]

#[macro_use]
extern crate structopt;
#[macro_use]
extern crate failure;
extern crate libc;
extern crate nix;
extern crate rustyline;

mod breakpoint;
mod debugger;
mod registers;

use debugger::Debugger;
use failure::Error;
use libc::execl;
use nix::{sys::ptrace::traceme,
          unistd::{fork, ForkResult}};
use std::ffi::CString;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "db", about = "A debugger designed for humans")]
struct Options {
    #[structopt(parse(from_str))]
    program: String,
}

fn main() {
    let opts = Options::from_args();
    if let Err(e) = drive(opts) {
        println!("{}", e);
    }
}

/// Drives the debugger by taking input commands and executing it
fn drive(opts: Options) -> Result<(), Error> {
    match fork() {
        Ok(ForkResult::Parent { child, .. }) => {
            println!("Child PID is {}", child);
            let mut debugger = Debugger::new(child, opts.program.clone());
            debugger.run()?;
        }
        Ok(ForkResult::Child) => {
            traceme()?;
            let prog = CString::new(opts.program.clone())?;
            // We need to make sure this ptr is not dropped too early so
            // we set a value here and pas it in to execl
            let prog_ptr = prog.as_ptr();
            let return_value = unsafe { execl(prog_ptr, prog_ptr) };
            println!("Return value of {} was {}", opts.program, return_value);
        }
        Err(e) => println!(
            "Failed to fork process to debug {}. Reason was: {}",
            &opts.program, e
        ),
    }

    Ok(())
}
